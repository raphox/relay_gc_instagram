# Generate by

```sh
create-react-app instagram
cd instagram/
npm install --save react-relay react-router-dom
graphcool init server
# Changed ./server/types.graphql
cd server
graphcool deploy
# Created ./src/createRelayEnvironment.js from https://github.com/graphcool-examples/react-graphql/blob/master/quickstart-with-relay-modern/src/createRelayEnvironment.js
# Replaced __RELAY_API_ENDPOINT__ on ./src/createRelayEnvironment.js Copied from https://console.graph.cool/<APP> on button `ENDPOINTS`
# Created ./schema.graphql from https://console.graph.cool/<APP> on button `EXPORT SCHEMA`
# Inserted `"relay": "relay-compiler --src ./src --schema ./schema.graphql"` on ./package.json
cd ..
npm install --save-dev babel-preset-react-app babel-plugin-relay relay-compiler graphql
# Created file .babelrc:
# {
#     "presets": [
#         "react-app"
#     ],
#     "plugins": [
#         ["relay"]
#     ]
# }
git init
npm run relay
# Created ./src/mutations/CreatePostMutation.js from https://github.com/graphcool-examples/react-graphql/blob/master/quickstart-with-relay-modern/src/mutations/CreatePostMutation.js
# Created ./src/mutations/DeletePostMutation.js from https://github.com/graphcool-examples/react-graphql/blob/master/quickstart-with-relay-modern/src/mutations/DeletePostMutation.js
npm run eject
```

# References

* https://facebook.github.io/relay/docs/en/installation-and-setup.html
* https://hackernoon.com/using-create-react-app-with-relay-modern-989c078fa892
* https://www.prisma.io/blog/getting-started-with-relay-modern-46f8de6bd6ec/
* https://www.graph.cool/docs/quickstart/frontend/react/relay-sot2faez6a
* https://codeburst.io/how-to-setup-a-react-native-graphql-relay-modern-a6a5f6c18353