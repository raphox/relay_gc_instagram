import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';

// components
import Home from './components/Home';
import CreatePage from './components/CreatePage';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/create" component={CreatePage} />
          <Route path="*" render={() => <h1>Not found</h1>} />
        </Switch>
      </Router>
    );
  }
}

export default App;
